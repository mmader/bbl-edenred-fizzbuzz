# Florilège de techniques de test en C#
## BBL - Edenred - 11/12/2020, présenté par [Maxime MADER](https://twitter.com/kawabytes)

### Le kata Fizz Buzz
Il s’agit d’un kata célèbre utilisé dans les entretiens techniques. Je vous propose de consulter [l’article issu du wiki de Cunningham & Cunningham](https://wiki.c2.com/?FizzBuzzTest) qui centralise quelques échanges et articles intéressants autour du kata et de son utilisation en entretien technique.

Voici les règles métiers :
- Write a program that prints one line for each number from 1 to 100
- For multiples of three print Fizz instead of the number
- For the multiples of five print Buzz instead of the number
- For numbers which are multiples of both three and five print FizzBuzz instead of the number

Il existe également [une variante intéressante appelée FooBarQix](https://codingdojo.org/kata/FooBarQix/) qui ajoute un peu de complexité, justifiant une implémentation plus élaborée.

### Approche “boîte noire”
L’idée de [cette approche](https://en.wikipedia.org/wiki/Black-box_testing), c’est d’examiner notre application sans connaissance de sa structure interne, sans fouiller dans les détails de son implémentation. Notre “boîte” logicielle étant fermée, nous devons nous contenter des entrées et des sorties directes. 

Généralement, les cas de tests sont élaborés à partir des spécifications et exigences de l’application.

### Approche “boîte blanche”
L’idée de [cette approche](https://en.wikipedia.org/wiki/White-box_testing), c’est d’examiner notre application en ayant connaissance de sa structure internes ainsi que des détails de son implémentation. Notre “boîte” logicielle étant ouverte, nous pouvons nous intéresser aux interactions entre les différentes briques de notre code, à la couverture du code, aux différentes branches de notre code, etc.

Bien que cette approche nous permet de tester notre code sous de nombreuses facettes, cette stratégie de tests apporte son lot de complexité : le code doit être raisonnablement testable, les tests ne doivent pas être couplés à une implémentation spécifique, les tests ne doivent pas être une duplication du code de l’implémentation, etc.

## Outils de test
### Tests unitaires avec xUnit
[Cet outil](https://xunit.net/) est à mon sens, la meilleure alternative à MS Test. xUnit nous permet d’utiliser assez facilement [les tests paramétrés](https://github.com/xunit/samples.xunit) ([documentation officielle](https://xunit.net/#documentation)).

Alternatives : [NUnit](https://nunit.org/)
Compléments : [Fluent Assertions](https://fluentassertions.com/)

### Golden Master avec ApprovalTests
[Cet outil](https://github.com/approvals/ApprovalTests.Net) est très pratique pour effectuer un différentiel sur vos descriptions de code, et permet également de tester des rendus ou d'automatiser une partie des tâches de contrôle
([documentation officielle](https://github.com/approvals/ApprovalTests.Net/blob/master/docs/ApprovalTests/readme.md)).

### Property-Based Testing avec FsCheck
[Cet outil](https://fscheck.github.io/FsCheck/) nous permet d’utiliser le [Property-Based Testing](https://fsharpforfunandprofit.com/posts/property-based-testing/) (PBT) en C#. FsCheck nous permet également de fonctionner avec ou sans le moteur de xUnit/NUnit selon nos envies.

### Double de tests avec Moq
[Cet outil](https://github.com/Moq/moq4/wiki/Quickstart) nous permet d’utiliser notamment des [mocks](https://en.wikipedia.org/wiki/Mock_object) afin de tester les interactions entre nos différents objets.

Alternatives : [NSubstitute](https://nsubstitute.github.io/)

### Poursuivre la découverte
- [AutoFixture](https://github.com/AutoFixture/AutoFixture) - AutoFixture is an open source framework for .NET designed to minimize the 'Arrange' phase of your unit tests
- [FakeItEasy](https://github.com/FakeItEasy/FakeItEasy) - The easy mocking library for .NET https://fakeiteasy.github.io
- [Machine.Specifications](https://github.com/machine/machine.specifications) - Machine.Specifications (MSpec) is a context/specification framework that removes language noise and simplifies tests.
- [Shouldly](https://github.com/shouldly/shouldly) - Shouldly is an assertion framework which focuses on giving great error messages when the assertion fails while being simple and terse.
- [Snapshooter](https://github.com/SwissLife-OSS/snapshooter) - A snapshot testing tool for .NET Core and .NET Framework
- [SpecFlow](https://github.com/SpecFlowOSS/SpecFlow) - Binding business requirements to .Net code
- Stryker.NET - Mutation testing for .NET Core projects
- [xBehave.net](https://github.com/adamralph/xbehave.net) - An xUnit.net extension for describing your tests using natural language. https://xbehave.github.io
- [Compare-Net-Objects](https://github.com/GregFinzer/Compare-Net-Objects) - Perform a deep compare of any two .NET objects using reflection. Shows the differences between the two objects.