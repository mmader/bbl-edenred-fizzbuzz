using FizzBuzz.ClosedBox.CLI;
using Xunit;

namespace FizzBuzz.ClosedBox.Tests
{
    public class FizzBuzzKataTests
    {
        [Fact]
        public void GivenAMultipleOf3ShouldReturnFizz()
        {
            // 1. Arrange
            const string expected = "Fizz";
            const int input = 3;

            // 2. Act
            var actual = Program.Compute(input);

            // 3. Assert
            Assert.Equal(expected, actual);
        }
        
        [Fact]
        public void GivenAMultipleOf5ShouldReturnBuzz()
        {
            // 1. Arrange
            const string expected = "Buzz";
            const int input = 5;

            // 2. Act
            var actual = Program.Compute(input);

            // 3. Assert
            Assert.Equal(expected, actual);
        }
        
        [Fact]
        public void GivenAMultipleOf3And5ShouldReturnFizzBuzz()
        {
            // 1. Arrange
            const string expected = "FizzBuzz";
            const int input = 15;

            // 2. Act
            var actual = Program.Compute(input);

            // 3. Assert
            Assert.Equal(expected, actual);
        }
        
        [Fact]
        public void GivenAnyOtherValueShouldReturnTheValue()
        {
            // 1. Arrange
            const string expected = "1";
            const int input = 1;

            // 2. Act
            var actual = Program.Compute(input);

            // 3. Assert
            Assert.Equal(expected, actual);
        }
        
        [Theory]
        [InlineData(3)]
        [InlineData(6)]
        [InlineData(9)]
        [InlineData(12)]
        //[InlineData(15)], uh oh
        [InlineData(18)]
        [InlineData(21)]
        public void GivenSomeMultipleOf3ShouldReturnFizz(int input)
        {
            // 1. Arrange
            const string expected = "Fizz";

            // 2. Act
            var actual = Program.Compute(input);

            // 3. Assert
            Assert.Equal(expected, actual);
        }
        
        [Theory]
        [InlineData(1, "1")]
        [InlineData(2, "2")]
        [InlineData(3, "Fizz")]
        [InlineData(4, "4")]
        [InlineData(5, "Buzz")]
        [InlineData(15, "FizzBuzz")]
        public void GivenSomeValueShouldReturnExpectedValue(int input, string expected)
        {
            // 1. Arrange via InlineData

            // 2. Act
            var actual = Program.Compute(input);

            // 3. Assert
            Assert.Equal(expected, actual);
        }
    }
}