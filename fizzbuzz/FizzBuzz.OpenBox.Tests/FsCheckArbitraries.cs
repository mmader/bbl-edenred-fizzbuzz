using System;
using FsCheck;
using FsCheck.Xunit;
using Microsoft.FSharp.Core;
using Xunit.Sdk;

namespace FizzBuzz.OpenBox.Tests
{
    // FsCheck arbitrary to generate out of range inputs :
    // - integers equal or less than 0.
    public static class ArgumentOutOfRangeArbitrary
    {
        public static Arbitrary<int> Ints() 
            => Arb.Default.Int32().Filter(x => x <= 0);
    }

    public sealed class ArgumentOutOfRangePropertyAttribute : PropertyAttribute
    {
        public ArgumentOutOfRangePropertyAttribute() 
            => Arbitrary = new[] { typeof(ArgumentOutOfRangeArbitrary) };
    }
    
    // FsCheck arbitrary for greater than 0 integers.
    public class StrictlyPositiveIntegerArbitrary
    {
        private readonly int _min;
        private readonly int _max;

        public StrictlyPositiveIntegerArbitrary(int min, int max)
        {
            _min = min;
            _max = max;
        }

        public Arbitrary<int> Ints() => Arb.Default.Int32().Filter(x => x > _min && x < _max);
    }
    
    public sealed class StrictlyPositiveIntegerPropertyAttribute : PropertyAttribute
    {
        public StrictlyPositiveIntegerPropertyAttribute(int min, int max) 
            => Arbitrary = new[] { new StrictlyPositiveIntegerArbitrary(min, max).GetType() };
    }
}