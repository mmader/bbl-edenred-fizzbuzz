using System;
using FizzBuzz.OpenBox.CLI;
using FsCheck;
using FsCheck.Xunit;
using Xunit;

namespace FizzBuzz.OpenBox.Tests
{
    public class DefaultFizzBuzzAlgorithmTests
    {
        [ArgumentOutOfRangeProperty]
        public void given_out_of_range_integers_should_throw_argument_out_of_range_exception(int outOfRangeValue)
        {
            // Arrange.
            var fizzBuzzAlgorithm = new DefaultFizzBuzzAlgorithm();

            // Act && assert.
            Assert.Throws<ArgumentOutOfRangeException>(() => fizzBuzzAlgorithm.Compute(outOfRangeValue));
        }
        
        [MultipleOf3AndNotMultipleOf5Property]
        public void given_multiple_of_3_and_not_multiple_of_5_should_return_Fizz(int value)
        {
            // Arrange.
            const string expected = "Fizz";
            
            var fizzBuzzAlgorithm = new DefaultFizzBuzzAlgorithm();

            // Act.
            var actual = fizzBuzzAlgorithm.Compute(value);
            
            // Assert.
            Assert.Equal(expected, actual);
        }
        
        // FsCheck arbitrary to generate "Fizz" inputs :
        // - integers greater than 0,
        // - integers multiple of 3,
        // - integers not multiple of 5.
        private static class MultipleOf3AndNotMultipleOf5Arbitrary
        {
            public static Arbitrary<int> Ints() 
                => Arb.Default.Int32().Filter(x => x > 0 && x % 3 == 0 && x % 5 != 0);
        }

        private sealed class MultipleOf3AndNotMultipleOf5PropertyAttribute : PropertyAttribute
        {
            public MultipleOf3AndNotMultipleOf5PropertyAttribute() 
                => Arbitrary = new[] { typeof(MultipleOf3AndNotMultipleOf5Arbitrary) };
        }
        
        [NotMultipleOf3AndMultipleOf5Property]
        public void given_not_multiple_of_3_and_multiple_of_5_should_return_Buzz(int value)
        {
            // Arrange.
            const string expected = "Buzz";
            
            var fizzBuzzAlgorithm = new DefaultFizzBuzzAlgorithm();

            // Act.
            var actual = fizzBuzzAlgorithm.Compute(value);
            
            // Assert.
            Assert.Equal(expected, actual);
        }
        
        // FsCheck arbitrary to generate "Buzz" inputs :
        // - integers greater than 0,
        // - integers not multiple of 3,
        // - integers multiple of 5.
        private static class NotMultipleOf3AndMultipleOf5Arbitrary
        {
            public static Arbitrary<int> Ints() 
                => Arb.Default.Int32().Filter(x => x > 0 && x % 3 != 0 && x % 5 == 0);
        }
            
        private sealed class NotMultipleOf3AndMultipleOf5PropertyAttribute : PropertyAttribute
        {
            public NotMultipleOf3AndMultipleOf5PropertyAttribute() 
                => Arbitrary = new[] { typeof(NotMultipleOf3AndMultipleOf5Arbitrary) };
        }
        
        [MultipleOf3AndMultipleOf5Property]
        public void given_multiple_of_3_and_multiple_of_5_should_return_FizzBuzz(int value)
        {
            // Arrange.
            const string expected = "FizzBuzz";
            
            var fizzBuzzAlgorithm = new DefaultFizzBuzzAlgorithm();

            // Act.
            var actual = fizzBuzzAlgorithm.Compute(value);
            
            // Assert.
            Assert.Equal(expected, actual);
        }
        
        // FsCheck arbitrary to generate "FizzBuzz" inputs :
        // - integers greater than 0,
        // - integers multiple of 3,
        // - integers multiple of 5.
        private static class MultipleOf3AndMultipleOf5Arbitrary
        {
            public static Arbitrary<int> Ints() 
                => Arb.Default.Int32().Filter(x => x > 0 && x % 3 == 0 && x % 5 == 0);
        }
            
        private sealed class MultipleOf3AndMultipleOf5PropertyAttribute : PropertyAttribute
        {
            public MultipleOf3AndMultipleOf5PropertyAttribute() 
                => Arbitrary = new[] { typeof(MultipleOf3AndMultipleOf5Arbitrary) };
        }
        
        [NotMultipleOf3AndNotMultipleOf5Property]
        public void given_not_multiple_of_3_and_not_multiple_of_5_should_return_the_number_as_string(int value)
        {
            // Arrange.
            var expected = value.ToString();
            
            var fizzBuzzAlgorithm = new DefaultFizzBuzzAlgorithm();

            // Act.
            var actual = fizzBuzzAlgorithm.Compute(value);
            
            // Assert.
            Assert.Equal(expected, actual);
        }
        
        // FsCheck arbitrary to generate number as string inputs :
        // - integers greater than 0,
        // - integers not multiple of 3,
        // - integers not multiple of 5.
        private static class NotMultipleOf3AndNotMultipleOf5Arbitrary
        {
            public static Arbitrary<int> Ints() 
                => Arb.Default.Int32().Filter(x => x > 0 && x % 3 != 0 && x % 5 != 0);
        }

        private sealed class NotMultipleOf3AndNotMultipleOf5PropertyAttribute : PropertyAttribute
        {
            public NotMultipleOf3AndNotMultipleOf5PropertyAttribute() 
                => Arbitrary = new[] { typeof(NotMultipleOf3AndNotMultipleOf5Arbitrary) };
        }
    }
}