using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using ApprovalUtilities.Utilities;
using FizzBuzz.OpenBox.CLI;
using FsCheck;
using Moq;
using Xunit;
using Random = FsCheck.Random;
using Range = Moq.Range;

namespace FizzBuzz.OpenBox.Tests
{
    public class FizzBuzzKataTests
    {
        [ArgumentOutOfRangeProperty]
        public void given_out_of_range_integers_should_throw_argument_out_of_range_exception(int outOfRangeValue)
        {
            // Arrange.
            const int inRangeValue = 1;
            
            var textWriterMock = new Mock<TextWriter>();
            var fizzBuzzAlgorithmMock = new Mock<IFizzBuzzAlgorithm>();
            
            var fizzBuzzKata = new FizzBuzzKata(fizzBuzzAlgorithmMock.Object, textWriterMock.Object);

            // Act && assert.
            fizzBuzzAlgorithmMock.VerifyNoOtherCalls();
            
            Assert.Throws<ArgumentOutOfRangeException>(
                () => fizzBuzzKata.PrintFizzBuzz(outOfRangeValue, inRangeValue));
            Assert.Throws<ArgumentOutOfRangeException>(
                () => fizzBuzzKata.PrintFizzBuzz(inRangeValue, outOfRangeValue));
            textWriterMock.VerifyNoOtherCalls();
        }
        
        [Theory]
        [MemberData(nameof(SampleFizzBuzzRanges))]
        public void for_each_value_in_range_should_call_compute_fizzbuzz_in_order_and_pass_its_return_value_to_the_text_writer(
            FizzBuzzRange fizzBuzzRange)
        {
            // Arrange.
            var (start, count) = fizzBuzzRange;
            
            var textWriterMock = new Mock<TextWriter>();
            var fizzBuzzAlgorithmMock = new Mock<IFizzBuzzAlgorithm>();

            var fizzBuzzKata = new FizzBuzzKata(fizzBuzzAlgorithmMock.Object, textWriterMock.Object);

            Enumerable.Range(start, count).ForEach(value =>
            {
                fizzBuzzAlgorithmMock
                    .Setup(x => x.Compute(value))
                    .Returns(value.ToString());
            });

            // Act.
            fizzBuzzKata.PrintFizzBuzz(start, count);

            // Assert.
            Enumerable.Range(start, count).ForEach(value =>
            {
                fizzBuzzAlgorithmMock.Verify(x => x.Compute(value), Times.Once);
                textWriterMock.Verify(x => x.WriteLine(value.ToString()), Times.Once);
            });
            
            fizzBuzzAlgorithmMock.VerifyNoOtherCalls();
            textWriterMock.VerifyNoOtherCalls();
        }

        public record FizzBuzzRange(int Start, int Count);
        
        public static IEnumerable<object[]> SampleFizzBuzzRanges =>
            new List<object[]>
            {
                new object[] { new FizzBuzzRange(1, 10) },
                new object[] { new FizzBuzzRange(3, 17) },
                new object[] { new FizzBuzzRange(5, 1) },
            };
    }
}