using System;
using System.IO;
using System.Text;
using ApprovalTests;
using ApprovalTests.Reporters;
using FizzBuzz.OpenBox.CLI;
using Xunit;
using Xunit.Abstractions;

namespace FizzBuzz.Open.Tests
{
    [UseReporter(typeof(DiffReporter))]
    public class GoldenMasterTests : XunitApprovalBase, IDisposable
    {
        private readonly TextWriter _originalOutTextWriter;
        
        [Fact]
        public void SampleGoldenMasterWithApprovalTests()
        {
            var sb = new StringBuilder();
            Console.SetOut(new StringWriter(sb));
            
            Program.Main(null);
            
            Approvals.Verify(sb.ToString());
        }

        public GoldenMasterTests(ITestOutputHelper output) : base(output) 
            => _originalOutTextWriter = Console.Out;

        void IDisposable.Dispose() 
            => Console.SetOut(_originalOutTextWriter);
    }
}