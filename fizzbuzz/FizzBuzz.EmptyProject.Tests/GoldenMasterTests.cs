using ApprovalTests;
using ApprovalTests.Reporters;
using Xunit;
using Xunit.Abstractions;

namespace FizzBuzz.EmptyProject.Tests
{
    [UseReporter(typeof(DiffReporter))]
    public class GoldenMasterTests : XunitApprovalBase
    {
        [Fact]
        public void SampleGoldenMasterWithApprovalTests()
        {
            Approvals.Verify("actual");
        }

        public GoldenMasterTests(ITestOutputHelper output) : base(output)
        {
        }
    }
}