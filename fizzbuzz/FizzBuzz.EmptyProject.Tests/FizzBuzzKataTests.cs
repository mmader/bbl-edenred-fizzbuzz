using FsCheck.Xunit;
using Xunit;

namespace FizzBuzz.EmptyProject.Tests
{
    public class FizzBuzzKataTests
    {
        [Fact]
        public void SampleFactWithXUnit()
        {
            Assert.False(true);
        }
        
        [Theory]
        [InlineData(1)]
        [InlineData(2)]
        [InlineData(3)]
        public void SampleTheoryWithXUnit(int input)
        {
            Assert.Equal(0, input);
        }
        
        [Property]
        public void SampleTestWithFsCheck(string input)
        {
            Assert.NotEqual(input, input);
        }
    }
}