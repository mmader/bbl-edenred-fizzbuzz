﻿using System;
using System.Linq;

namespace FizzBuzz.ClosedBox.CLI
{
    public class Program
    {
        public static void Main(string[] args)
        {
            foreach (var value in Enumerable.Range(1, 100))
            {
                Console.WriteLine(Compute(value));
            }
        }

        public static string Compute(int value)
        {
            var isMultipleOf3 = value % 3 == 0;
            var isMultipleOf5 = value % 5 == 0;

            return (isMultipleOf3, isMultipleOf5) switch
            {
                (true, true) => "FizzBuzz",
                (true, false) => "Fizz",
                (false, true) => "Buzz",
                _ => value.ToString()
            };
        }
    }
}