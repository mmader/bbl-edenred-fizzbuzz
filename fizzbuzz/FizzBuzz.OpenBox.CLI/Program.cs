﻿using System;
using System.Linq;

namespace FizzBuzz.OpenBox.CLI
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var fizzBuzzAlgorithm = new DefaultFizzBuzzAlgorithm();
            var textWriter = Console.Out;
            
            var fizzBuzzKata = new FizzBuzzKata(fizzBuzzAlgorithm, textWriter);
            
            fizzBuzzKata.PrintFizzBuzz(1, 100);
        }
    }
}