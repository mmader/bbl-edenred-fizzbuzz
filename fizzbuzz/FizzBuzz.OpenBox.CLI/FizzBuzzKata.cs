using System;
using System.IO;
using System.Linq;

namespace FizzBuzz.OpenBox.CLI
{
    public class FizzBuzzKata
    {
        private readonly IFizzBuzzAlgorithm _fizzBuzzAlgorithm;
        private readonly TextWriter _textWriter;

        public FizzBuzzKata(IFizzBuzzAlgorithm fizzBuzzAlgorithm, TextWriter textWriter)
        {
            _fizzBuzzAlgorithm = fizzBuzzAlgorithm;
            _textWriter = textWriter;
        }
        
        public void PrintFizzBuzz(int start, int count)
        {
            GuardAgainstOutOfRangeArgument(start);
            GuardAgainstOutOfRangeArgument(count);
            
            foreach (var value in Enumerable.Range(start, count))
            {
                _textWriter.WriteLine(_fizzBuzzAlgorithm.Compute(value));
            }
        }
        
        private static void GuardAgainstOutOfRangeArgument(int input)
        {
            if (input <= 0)
                throw new ArgumentOutOfRangeException(nameof(input), input, $"{nameof(input)} is less than 1");
        }
    }
}