namespace FizzBuzz.OpenBox.CLI
{
    public interface IFizzBuzzAlgorithm
    {
        string Compute(int value);
    }
}