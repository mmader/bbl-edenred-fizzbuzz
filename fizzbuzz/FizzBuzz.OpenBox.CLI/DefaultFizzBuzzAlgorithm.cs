using System;

namespace FizzBuzz.OpenBox.CLI
{
    public class DefaultFizzBuzzAlgorithm : IFizzBuzzAlgorithm
    {
        public string Compute(int value)
        {
            if (value <= 0)
                throw new ArgumentOutOfRangeException(nameof(value), value, $"{nameof(value)} is less than 1");
            
            var isMultipleOf3 = value % 3 == 0;
            var isMultipleOf5 = value % 5 == 0;

            return (isMultipleOf3, isMultipleOf5) switch
            {
                (true, true) => "FizzBuzz",
                (true, false) => "Fizz",
                (false, true) => "Buzz",
                _ => value.ToString()
            };
        }
    }
}